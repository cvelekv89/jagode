import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HttpModule } from "@angular/http";

import { AppComponent } from "./app.component";
import { HomeView } from "./routes/HomeView";
import { JednaJagoda } from "./routes/JednaJagoda";
import { JagodeView } from "./routes/JagodeView";
import { Menu } from "./components/menu";
import { Info } from "./routes/jednajagoda/info";
import { Edit } from "./routes/jednajagoda/edit";
import { JagodeService } from "./services/jagode.service";
import { JagodeModel  } from './services/jagode.model';
import { Delete } from './routes/jednajagoda/delete';
import { NewJagoda } from './routes/newjagoda';

import { FormsModule } from '@angular/forms';
import { FilterPipePipe } from './filter.pipe'

const routes: Routes = [
  { path: "", component: HomeView },
  { path: "jagode", component: JagodeView },
  { path: "jagoda/new", component: NewJagoda },
  { path: "jagode/:id",
    component: JednaJagoda,
    children: [
      { path: "", redirectTo: "info", pathMatch: "full" },
      { path: "edit", component: Edit },
      { path: "info", component: Info },
      { path: "delete", component: Delete }
    ]
  },
  { path: "**", redirectTo: "/" }
  
];

@NgModule({
  declarations: [
    AppComponent,
    HomeView,
    JednaJagoda,
    JagodeView,
    Menu,
    Info,
    Edit,
    Delete,
    NewJagoda,
    FilterPipePipe
  ],
  imports: [BrowserModule, 
    RouterModule.forRoot(routes),
    HttpModule,
    FormsModule
  ],

  providers: [
    JagodeService,
    JagodeModel
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
