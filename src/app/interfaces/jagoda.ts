export interface Jagoda {
    id:number,
    nutritionValue:number,
    weight:string,
    color?:string
}