export const colors = [
    {
      label: "Green",
      key: "green"
    },
    {
      label: "Blue",
      key: "blue"
    },
    {
      label: "Red",
      key: "red"
    },
    {
      label: "Orange",
      key: "orange"
    },
    {
      label: "Pink",
      key: "pink"
    }
  ];

