import { Component } from '@angular/core';

@Component({
    selector: 'menu-list',
    templateUrl: './menu.html'
})

export class Menu{
    links = [
        {
            name:'Home',
            path:'/',
            active: false
        },
        {
            name:'Jagode',
            path:'/jagode',
            active: false
        },
        {
            name:'Add jagodu',
            path:'/jagoda/new',
            active: false
        }
        
    ]
}