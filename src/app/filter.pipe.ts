import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "filterPipe"
})
export class FilterPipePipe implements PipeTransform {
  transform(values: any[], searchValueDrop:any, searchString: any): any {
    return searchString ? values.filter(el => {
      let result = false;
      if (String(el.nutritionValue).toLowerCase().indexOf(searchString.toLowerCase()) > -1){
        result = result || true;
      }
      if (String(el.weight).toLowerCase().indexOf(searchString.toLowerCase()) > -1){
        result = result || true;
      }
      if (String(el.color).toLowerCase().indexOf(searchString.toLowerCase()) > -1){
        result = result || true;
      }
      return result;
    }):values;
  }
}
