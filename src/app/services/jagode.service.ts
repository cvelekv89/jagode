import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";

@Injectable()
export class JagodeService {
  constructor(private http: Http) {}

  getJagodeObservable() {
    return this.http.get("http://localhost:3000/jagode").map(response => {
      return response.json();
    });
  }
  getJednaJagodaObservable(id) {
    return this.http.get("http://localhost:3000/jagode/"+id).map(response => {
      return response.json();
    });
  }
  addJednaJagodaObservable(jagoda) {
    return this.http.post("http://localhost:3000/jagode",jagoda).map(response => {
      return response.json();
    });
  }
  updateJednaJagodaObservable(jagoda) {
    return this.http.put("http://localhost:3000/jagode/"+jagoda.id,jagoda).map(response => {
      return response.json();
    });
  }

  deleteJednaJagodaObservable(id){
    return this.http.delete("http://localhost:3000/jagode/"+id).map(response => {
        return response.json();
      });
  }
}
