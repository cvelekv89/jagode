import { Injectable } from "@angular/core";
import { Jagoda } from "../interfaces/jagoda";
import { JagodeService } from "../services/jagode.service";


@Injectable()
export class JagodeModel {
  jagode: Jagoda[] = [];

  constructor(private service: JagodeService) {
    this.napuniJagode();
  }

  obrisiJagodu(id, clbk) {
    this.service.deleteJednaJagodaObservable(id).subscribe(() => {
      this.napuniJagode();
      clbk();
    });
  }
  napuniJagode() {
    this.service.getJagodeObservable().subscribe(jagode => {
      this.jagode = jagode;
    });
  }

  dodajJagodu(jagoda,clbk) {
    this.service.addJednaJagodaObservable(jagoda).subscribe(jagoda => {
      this.jagode.push(jagoda);
      clbk();
    });
  }

  vratiJednuJagodu(id,clbk){
    for(var i=0;i<this.jagode.length;i++){
      if(this.jagode[i].id == id){
        clbk(this.jagode[i]);
      }
    }
  }
  initializeJagodeWithCallback(clbk){
    if(!this.jagode || this.jagode.length < 1){
      this.service.getJagodeObservable().subscribe((jagode)=>{
        this.jagode = jagode;
        clbk();
        
      })
    }else{
      clbk()
    }
  }

  updateJagoda(jagoda){
    this.service.updateJednaJagodaObservable(jagoda).subscribe((jagoda)=>{
     this.napuniJagode();
    });
  }
}
