import { Component } from "@angular/core";
import { JagodeModel } from "../services/jagode.model";
import { Router } from "@angular/router";
import { colors } from "../components/configs/colors";

@Component({
  templateUrl: "./newjagoda.html"
})
export class NewJagoda {
  constructor(private jagodeModel: JagodeModel, private router: Router) {}

  colors = colors;
  jagoda = {};

  dodajJagodu() {
    if (this.jagoda['weight'] && this.jagoda['nutritionValue']) {
      this.jagodeModel.dodajJagodu(this.jagoda, () => {
        this.router.navigate(['/jagode']);
      });
    } else {
      alert("Nisu dobri unosi");
    }
  }
}
