import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
@Component({
  templateUrl: "./jednajagoda.html"
})
export class JednaJagoda {
  id;
  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.id = params["id"];
    });
  }
}
