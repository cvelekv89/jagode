import { Component } from "@angular/core";
import { JagodeModel } from "../services/jagode.model";
import { Router } from "@angular/router";
import { colors } from "../components/configs/colors";

@Component({
  templateUrl: "./jagodeview.html"
})
export class JagodeView {
  
  colors = colors;
  constructor(public jagodeModel: JagodeModel, private router: Router) {}

  otvoriJagodu(id) {
    this.router.navigate(["/jagode", id]);
  }
  editujJagodu(id) {
    this.router.navigate(["/jagode", id, "edit"]);
  }
  deleteJagoda(id) {
    this.router.navigate(["/jagode", id, "delete"]);
  }
}
