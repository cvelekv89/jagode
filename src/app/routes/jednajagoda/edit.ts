import { Component } from "@angular/core";
import { colors } from "../../components/configs/colors";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { JagodeModel } from "../../services/jagode.model";

@Component({
  templateUrl: "./edit.html"
})
export class Edit {
  id;
  nutritionValue;
  weight;
  boja;
  jagoda = {};

  colors = colors;

  constructor(
    private router: Router,
    public aRoute: ActivatedRoute,
    public jagodeModel: JagodeModel
  ) {}

  ngOnInit() {
    this.aRoute.parent.params.subscribe(({ id }) => {
      this.id = id;
    });

    this.jagodeModel.initializeJagodeWithCallback(() => {
      this.setJagodaVars();
    });
  }

  setJagodaVars() {
    this.jagodeModel.vratiJednuJagodu(
      this.id,
      ({ nutritionValue, weight, color }) => {
        this.nutritionValue = nutritionValue;
        this.weight = weight;
        this.boja = color;
      }
    );
    console.log(this.nutritionValue, this.weight, this.boja);
  }
  sacuvajJagodu() {
    const { id, nutritionValue, weight, boja } = this;
    this.jagodeModel.updateJagoda({ id, nutritionValue, weight, color:boja });
    this.router.navigate(["/jagode"]);
  }
}
