import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { JagodeModel } from "../../services/jagode.model";
import { Router } from '@angular/router'; 
@Component({
  templateUrl: "./delete.html"
})
export class Delete implements OnInit {
  id;

  constructor(
    private route: ActivatedRoute,
    private jagodeModel: JagodeModel,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.parent.params.subscribe(params => {
      this.id = params.id;
    });
  }

  deleteJagoda() {
      this.jagodeModel.obrisiJagodu(this.id,()=>{
          this.router.navigate(['/jagode']);
      });
  }
}
