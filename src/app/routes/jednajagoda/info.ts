import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { JagodeModel } from "../../services/jagode.model";
@Component({
  selector: "jagoda-info",
  templateUrl: "./info.html"
})
export class Info {
  id;
  nutritionValue;
  weight;
  boja;
  
  constructor(public aRoute: ActivatedRoute, public jagodeModel: JagodeModel) {}

  ngOnInit() {
    this.aRoute.parent.params.subscribe(({ id }) => {
      this.id = id;
    });
    this.jagodeModel.initializeJagodeWithCallback(() => {
      this.setJagodaVars();
    });
  }

  setJagodaVars() {
    this.jagodeModel.vratiJednuJagodu(
      this.id,
      ({ nutritionValue, weight, color }) => {
        this.nutritionValue = nutritionValue;
        this.weight = weight;
        this.boja = color;
      }
    );
  }
}
